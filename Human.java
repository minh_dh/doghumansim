public class Human{
    private String name;
    private double money;
    private int hunger;
    private int boredom;

    public Human(String name){
        this.name = name;
        this.money = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean doWork(){
        if(this.hunger<50 && this.boredom<50){
            System.out.println("Human worked succesfully.");
            this.money += 20;
            this.hunger += 5;
            this.boredom += 10;
            return true;
        }
        if(this.hunger > 50){
            System.out.println("Human too hungry to work.");
        }
        if(this.boredom > 50){
            System.out.println("Human too bored to work.");
        }
        return true;
    }
    
    public void eat(){
        this.hunger -= 30;
        if(this.hunger < 0){
            this.hunger = 0;
        }
        System.out.println("Human just ate.");
    }

    public boolean feed(Dog dog){
        if(this.money < 50){
            System.out.println("Human too broke.");
        }
        this.money -= 50;
        System.out.println("Human bought food.");
        eat();
        dog.eat();
        return true;
    }

    public void play(){
        if (this.boredom < 30){
            this.boredom = 0;
        }else{
            this.boredom -= 30;
        }
        this.hunger += 5;
        System.out.println("The human played with the dog");
    }

}