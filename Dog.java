public class Dog{

    private String name;
    private int energy;
    private int hunger;
    private int boredom;

    public Dog(String name){
        this.name = name;
        this.energy = 0;
        this.hunger = 0;
        this.boredom = 0;
    }

    public boolean takeNap(){
        if (hunger < 50 && boredom < 50){
            this.energy += 20;
            this.hunger += 5;
            this.boredom += 10;

            System.out.println("The dog napped succesfully");
            return true;
        }else{
            if (hunger > 50){
                System.out.println("The dog is too hungry too nap");
                return false;    
            }else{
                System.out.println("The dog is too bored to nap");
                return false;
            }    
        }
    }
    
    public void eat(){
        this.hunger -= 30;
        if(this.hunger < 0){
            this.hunger = 0;
        }
        System.out.println("Dog just ate.");
    }
    
    public void play(){
        if (this.boredom < 30){
            this.boredom = 0;
        }else{
            this.boredom -= 30;
        }
        this.hunger += 5;
        System.out.println("The dog played with the human");
    }

    public boolean playWith(Human human){
        if (this.energy < 50){
            System.out.println("The dog doesn't have enough energy to play");
            return false;
        }else{
            this.energy -= 50;
            play();
            human.play();
            return true;
        }
    }
}