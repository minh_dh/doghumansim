public class DogOwnerSim {
    public static void main(String[] args){
        Dog dog = new Dog("Doogy");

        int napNum = 0;
        int playNum = 0;

        while (napNum < 6){
            dog.takeNap();
            napNum++;
        }

        while (playNum < 3){
            dog.playWith(human);
            playNum++;
        }
    }
}
